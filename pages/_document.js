import React from 'react'
import Document, { Head, Main, NextScript } from 'next/document'

export default class extends Document {
  static async getInitialProps (...args) {
    const documentProps = await super.getInitialProps(...args)
    return { ...documentProps}
  }

  render () {
    return (<html>
      <Head>
        <link rel="stylesheet" href="https://unpkg.com/bulmaswatch/default/bulmaswatch.min.css"/>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.0/css/bulma.min.css"/>
        <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
      </Head>
      <body>
        <Main />
        <NextScript />
      </body>
    </html>)
  }
}
