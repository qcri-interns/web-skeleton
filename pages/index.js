import React from 'react'
import Head from 'next/head'

const data = {
  'articles': [{
    'title': 'article 1',
    'sentences': [
      {
        'related': [
          {
            'article': 2,
            'sentence': 1,
          }
        ],
        'text': 'sentence 1',
      }, {
        'related': [],
        'text': 'sentence 2',
      }, {
        'related': [],
        'text': 'sentence 3',
      }
    ]
  }, {
    'title': 'article 2',
    'sentences': [
      {
        'related': [
          {
            'article': 1,
            'sentence': 2,
          }
        ],
        'text': 'sentence 1',
      }, {
        'related': [],
        'text': 'sentence 2',
      }, {
        'related': [],
        'text': 'sentence 3',
      }
    ]
  }],
};

class Home extends React.Component {
  constructor() {
    super();
    this.state = {
      selectedArticle: 0,
    }
  }

  render() {
    const articles = data.articles.map((article, i) => 
      <li key={`article-${i}`}>
        <a className={this.state.selectedArticle==i?"is-active":""}
          onClick={() => { this.setState({selectedArticle:i})}}
        > {article.title} </a>
      </li>
    );
    if (this.state.expanded !== null) {
      const article = data.articles[this.state.selectedArticle];
      const related = article[this.state.expanded].sentence.related.map((related, i) =>
        
      );
    }
    const expanded = data.
    const senteces = data.articles[this.state.selectedArticle].sentences.map((sentence, i) => 
      <div>
      <p>
        {sentence.related.length>0?
          <a onClick={() => { this.setState({expanded:i}) }}><span className="icon"><i className="fas fa-angle-double-down"></i></span></a>
          :<span className="icon">&nbsp;</span>}
        {sentence.text}
      </p>
      { this.state.expanded==i &&

      }
      </div>
    );
    return (
      <div className="container">
        <Head>
          <title> DEMO </title>
          <link rel="icon" href="/favicon.ico" />
        </Head>

        <main>
          <h1 className="title">
            News Story Headline
          </h1>

          <div className="columns">
            <div className="column is-one-fifth">
            <aside className="menu">
                <p className="menu-label">
                  Articles
                </p>
              <ul className="menu-list">
                {articles}
              </ul>
            </aside> 
            </div>
            <div className="column">
              <div className="box">
                {senteces}
              </div>
            </div>
          </div>
        </main>

        <footer>
          Copyright (C) 2020, Qatar Computing Research Institute
        </footer>

      </div>
    )
  }
}

export default Home;